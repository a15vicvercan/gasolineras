# Refuel

### Que es Refuel?

Refuel es un portal web que muestra información sobre las gasolineras de Cataluña, sus precios en tiempo real junto con la evolución de sus precios durante el último mes

### Que pretende proporcionar esta página?

Refuel tiene tres funcionalidades:
	- Mostrar en un mapa todas las gasolineras con sus precios en tiempo real.
	- Mostrar una tabla con toda la información de cada gasolinera.
	- Gráficos que muestran la evolución de los precios durante el último mes y la jornada actual.
	
### Funcionalidades extra

	- Filtrar por marca de gasolineras.
	- Mostrar las gasolineras que se encuentran en un radio de 2,5km de tu posición actual.
	- Gráficos sobre el historial de precios segun el tipo de gasolina.
	
## Tecnologies usadas en el projecte

	* Node
	* Mongo
	* HTML
	* CSS
	* JS (AJAX)
	* API: Datatables, Geoportal (Gobierno), Leaflet (Mapa), Chart.js (gráficos) y ShareThis (botones compartir).
	
## Diseño

La página consiste en una sola página (single page) con varias “pestañas” con los apartados siguientes:

	- Mapa: se muestran todas las gasolineras de Cataluña en el mapa con sus precios, horario y una opcion de como llegar que muestra la ruta.
	- Tabla: tabla generada con la api de datatables que contiene los precios, dirección y localización de las gasolineras
	- Gráficos: Muestran la evolución del último mes de los precios de los distintos tipos de gasolinas y la media de los precios actuales.
	- Contacto: Apartado donde mostramos los datos de contacto y botones para compartir la página.

## Links de la web

- [Labs Victor Vera](https://labs.iam.cat/~a15vicvercan/gasolineras/SRC/Web)
- [Labs Cristian Ayala](https://labs.iam.cat/~a17criayafer/gasolineras/SRC/Web)
- [Labs Marc Nolla](https://labs.iam.cat/~a15marnolsab/gasolineras/SRC/Web)


[Moqup](https://app.moqups.com/GsTN0PpDPj/view/page/aa9df7b72)