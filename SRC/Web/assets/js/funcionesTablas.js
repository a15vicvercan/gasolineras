/**
 * Funcion que recibe un objeto gasolinera y los va insertando en un array
 * @author Cristian Ayala, Marc Nolla, Victor Vera
 * @param  {object} gasolineras - Objeto que contine toda la información de una gasolinera
 */
function pasarDatosArray(gasolineras) {
    let array = [];
    let arrayDatos = [];

    if (gasolineras.length > 0) {
        let mejorPrecio95 = gasolineras[0].pre95;
        let mejorPrecio98 = gasolineras[0].pre98;
        let mejorPrecioDi = gasolineras[0].preDi;
        let mejorPrecioDiA = gasolineras[0].preDiA;
    
        let aux1 = 0;
        let aux2 = 0;
        let aux3 = 0;
        let aux4 = 0;
    
        for (let i = 0; i < gasolineras.length; i++) {
    
            if (gasolineras[i].pre95 < mejorPrecio95) {
                mejorPrecio95 = gasolineras[i].pre95;
                
                aux1 = i;
            }
    
            if (gasolineras[i].pre98 < mejorPrecio98) {
                mejorPrecio98 = gasolineras[i].pre98;
                
                aux2 = i;
            }
    
            if (gasolineras[i].preDi < mejorPrecioDi) {
                mejorPrecioDi = gasolineras[i].preDi;
                
                aux3 = i;
            }
    
            if (gasolineras[i].preDiA < mejorPrecioDiA) {
                mejorPrecioDiA = gasolineras[i].preDiA;
                
                aux4 = i;
            }
        }
    
        for (let i = 0; i < gasolineras.length; i++) {
            let gas95;
            let gas98;
            let gasDi;
            let gasDiA;
    
            if (i == aux1) {
                gas95 = (gasolineras[i].pre95 + "&#9733");
            }
            else {
                gas95 = gasolineras[i].pre95;
            }
            
            if (i == aux2){
                gas98 = (gasolineras[i].pre98 + "&#9733");
            }
            else {
                gas98 = gasolineras[i].pre98;
            }
    
            if (i == aux3){
                gasDi = (gasolineras[i].preDi + "&#9733");
            }
            else {
                gasDi = gasolineras[i].preDi;
            }
    
            if (i == aux4){
                gasDiA = (gasolineras[i].preDiA + "&#9733");
            }
            else {
                gasDiA = gasolineras[i].preDiA;
            }
    
            arrayDatos = [gasolineras[i].rotulo, gas95, gas98, gasDi, gasDiA, gasolineras[i].direccion, gasolineras[i].localidad];
            array.push(arrayDatos);
        }
    
        return array;
    }

}
/**
 * Funcion que crea la tabla de las gasolineras
 * @author Cristian Ayala, Marc Nolla, Victor Vera
 * @param  {array} dataSet
 */
function crearTable(dataSet) {

    let table = $('#tabla').DataTable({
        data: dataSet,
        "destroy": true,
        "sScrollY": "100%",
        "sScrollX": "100%",
        "language": {
            "lengthMenu": "Mostrar _MENU_ campos por pagina",
            "zeroRecords": "No hay resultados",
            "emptyTable": "No hay datos disponibles en la tabla",
            "info": "Pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No hay ningun dato disponible",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "search": "Buscar: ",
            "infoPostFix": "",
            "decimal": "",
            "lengthMenu": "Mostrar: _MENU_",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "paginate": {
                "first": "Primero",
                "last": "Última",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": activar para ordenar la columna ascendente",
                "sortDescending": ": activar para ordenar la columna descendente"
            }
        },
        columns: [
            { title: "Marca" },
            { title: "Sin plomo 95" },
            { title: "Sin plomo 98" },
            { title: "Diésel" },
            { title: "Diésel Plus" },
            { title: "Dirección" },
            { title: "Municipio" }
        ]
    });

    $('#tabla tbody').on('click', 'tr', function () {
        let rowData = table.row(this).data();

        console.log(rowData)

        let obj;
        let latitud;
        let longitud;

        for (let i = 0; i < window.gasolineras.length; i++) {
            if (rowData[5] == window.gasolineras[i]["Dirección"]) {

                obj = procesarGasolineras(gasolineras[i])

                latitud = obj.lat;
                longitud = obj.long;
            }
        }

        window.map.setView([latitud, longitud], 15);

        mostrarMarcadores(obj.lat, obj.long, map, obj.rotulo, obj.horario, obj.pre95, obj.pre98, obj.preDi, obj.preDiA, 1);

        $('html,body').animate({
            scrollTop: $("#map").offset().top
        }, 2000);

    });

}