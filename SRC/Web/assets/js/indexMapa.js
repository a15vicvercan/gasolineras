//Iniciamos la ruta
window.ruta;
window.rutaExistente = 0;

let fechaHora;

/**
 * Funcion que genera los marcadores de cada gasolinera en el mapa
 * @author Cristian Ayala, Marc Nolla, Victor Vera
 * @param  {int} lat - La latitud
 * @param  {int} long - La longitud
 * @param  {} map - El mapa
 * @param  {string} rotulo - El nombre de la gasolinera
 * @param  {string} horario - El horario
 * @param  {int} pre95 - Precio de la sin plomo 95
 * @param  {int} pre98 - Precio de la sin plomo 98
 * @param  {int} preDi - Precio del diésel
 * @param  {int} preDiA - Precio del dieésal plus
 * @param  {} aux
 */
function mostrarMarcadores(lat, long, map, rotulo, horario, pre95, pre98, preDi, preDiA, aux) {
    let textoPopUp = "<div class ='letrasMapa'> <h2 class='rotulo'><b>" + rotulo + "</b></h2><br><span class = 'horario'>Horario:</span> " + horario + "<br><hr>";
    if (pre95 == "No disponible") {
        textoPopUp += "<span class ='gasolina95'>Gasolina 95:</span> " + pre95 + "<br>";
    }
    else {
        textoPopUp += "<span class ='gasolina95'>Gasolina 95:</span> " + pre95 + "&euro;<br>";
    }
    if (pre98 == "No disponible") {
        textoPopUp += "<span class ='gasolina98'>Gasolina 98:</span> " + pre98 + "<br>";
    }
    else {
        textoPopUp += "<span class ='gasolina98'>Gasolina 98:</span> " + pre98 + "&euro;<br>";
    }
    if (preDi == "No disponible") {
        textoPopUp += "<span class ='diesel'>Diesel:</span> " + preDi + "<br>";
    }
    else {
        textoPopUp += "<span class ='diesel'>Diesel:</span> " + preDi + "&euro;<br>";
    }
    if (preDiA == "No disponible") {
        textoPopUp += "<span class ='dieselPlus'>Diesel Plus:</span> " + preDiA + "<br>";
    }
    else {
        textoPopUp += "<span class ='dieselPlus'>Diesel Plus:</span> " + preDiA + "&euro;<br>";
    }

    textoPopUp += "<a class = 'comoLlegar' onclick='comoLlegar(" + lat + "," + long + ")'>Como llegar</a>"

    textoPopUp += "</div>";

    var greenIcon = new L.Icon({
        iconUrl: 'assets/images/marker-icon-2x-green.png',
        shadowUrl: 'assets/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });

    var redIcon = new L.Icon({
        iconUrl: 'assets/images/marker-icon-2x-red.png',
        shadowUrl: 'assets/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });

    if (aux) {
        if (horario.length == 8) {
            L.marker([lat, long], { icon: greenIcon }).addTo(map)
                .bindPopup(textoPopUp)
                .openPopup();
        }
        else {
            L.marker([lat, long], { icon: redIcon }).addTo(map)
                .bindPopup(textoPopUp)
                .openPopup();
        }
    }
    else {
        if (horario.length == 8) {
            L.marker([lat, long], { icon: greenIcon }).addTo(map)
                .bindPopup(textoPopUp);
        }
        else {
            L.marker([lat, long], { icon: redIcon }).addTo(map)
                .bindPopup(textoPopUp);
        }
    }

}
/**
 * Funcion que genera la ruta desde la posicion actual a la posicion de la gasolinera
 * @author Cristian Ayala, Marc Nolla, Victor Vera
 * @param  {int} latDestino - La latitud del punto de destino
 * @param  {int} longDestino - La longitud del punto de destino
 */
function comoLlegar(latDestino, longDestino) {

    if (compartirUbicacion) {
        if (window.rutaExistente) {

            map.removeControl(window.ruta);
        }

        window.ruta = L.Routing.control({
            waypoints: [
                L.latLng(latitudAct, longitudAct),
                L.latLng(latDestino, longDestino)
            ],
            collapsible: true,
            createMarker: function () { return null; },
            language: 'es',
            profile: 'car'
        })
            .addTo(map);

        window.rutaExistente = 1;
    }
    else {
        navigator.geolocation.getCurrentPosition(function (position) {
            window.latitudAct = position.coords.latitude;
            window.longitudAct = position.coords.longitude;

            window.map.setView([position.coords.latitude, position.coords.longitude], 15);

            var blueIcon = new L.Icon({
                iconUrl: 'assets/images/marker-icon-2x-blue.png',
                shadowUrl: 'assets/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });

            L.marker([position.coords.latitude, position.coords.longitude], { icon: blueIcon }).addTo(map)
                .bindPopup('Estas aquí')
                .openPopup();

            compartirUbicacion = 1;

            comoLlegar(latDestino, longDestino);
        })
    }
}
/**
 * Funcion que crea y actualiza los marcadores de las gasolineras del mapa
 * @author Cristian Ayala, Marc Nolla, Victor Vera
 * @param  {string} fechaHora - Indica la ultima fecha y hora a la que se ha actualizado los precios por ultima vez
 * @param  {string} marca - Indica la marca de las gasolineras que queremos filtrar
 * @param  {int} ubicacion - Indica si la ubicación sé está compartiendo o no
 */
function actualitzarMarcadors(fechaHora, marca, ubicacion) {

    let URL;

    if (marca == "todas") {
        URL = "https://api.mlab.com/api/1/databases/gasolineras/collections/gasolinera?q={'FechaHora': '" + fechaHora + "'}&l=1600&apiKey=DOZZAytBV3N46Futm-V4gJcR-3SdUIqb";
    }
    else {
        URL = "https://api.mlab.com/api/1/databases/gasolineras/collections/gasolinera?q={'FechaHora': '" + fechaHora + "', 'Rótulo': '" + marca + "'}&l=1600&apiKey=DOZZAytBV3N46Futm-V4gJcR-3SdUIqb";
    }

    $.ajax({
        url: URL,
        headers: {
            "x-requested-with": "xhr"
        },
        ContentType: 'application/json',

        success: function (response) {

            if ($('#contenedor-mapa').html()) {
                $("#map").remove();
                $('#contenedor-mapa').html("<div id='map'></div>");
            } else {
                $('#contenedor-mapa').html("<div id='map'></div>");
            }

            window.map = L.map('map');

            //Si el usurio no acepta la geolocalizacion, se mostrara en el mapa la vista de toda cataluña
            window.map.setView([41.837, 1.539], 9);

            //Iniciamos la ruta
            window.ruta = "";

            L.tileLayer('https://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);

            /*Legend specific*/
            var legend = L.control({ position: "bottomleft" });

            legend.onAdd = function (map) {
                var div = L.DomUtil.create("div", "legend");
                div.innerHTML += '<h4 class="tituloLeyenda">Leyenda</h4>';
                div.innerHTML += '<span class="leyenda"><img src="assets/images/azul.png" alt="Color azul">Posición actual</span><br>';
                div.innerHTML += '<span class="leyenda"><img src="assets/images/verde.png" alt="Color verde">24H</span><br>';
                div.innerHTML += '<span class="leyenda"><img src="assets/images/rojo.png" alt="Color rojo">Horaio variable</span><br>';

                return div;
            };

            legend.addTo(map);

            //Guardamos el array con todas las gasolinares
            window.gasolineras = response;

            let obj;

            if (ubicacion) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    //Si el usuario acepta compartir su ubicacion se ejecutara esta parte
                    window.latitudAct = position.coords.latitude;
                    window.longitudAct = position.coords.longitude;

                    window.map.setView([position.coords.latitude, position.coords.longitude], 14);

                    var blueIcon = new L.Icon({
                        iconUrl: 'assets/images/marker-icon-2x-blue.png',
                        shadowUrl: 'assets/images/marker-shadow.png',
                        iconSize: [25, 41],
                        iconAnchor: [12, 41],
                        popupAnchor: [1, -34],
                        shadowSize: [41, 41]
                    });

                    var circle = L.circle([position.coords.latitude, position.coords.longitude], {
                        color: 'blue',
                        fillColor: '#949494',
                        fillOpacity: 0.5,
                        radius: 2550
                    }).addTo(map);

                    L.marker([position.coords.latitude, position.coords.longitude], { icon: blueIcon }).addTo(map)
                        .bindPopup('Estas aquí')
                        .openPopup();

                    window.compartirUbicacion = 1;

                    let arrayObjetos = [];
                    let dataSet = [];

                    for (let i = 0; i < gasolineras.length; i++) {

                        obj = procesarGasolineras(gasolineras[i])

                        if (obj.lat >= (latitudAct - 0.022727273) && obj.lat <= (latitudAct + 0.022727273) && obj.long >= (longitudAct - 0.022727273) && obj.long <= (longitudAct + 0.022727273)) {

                            arrayObjetos.push(obj);
                        }

                    }

                    dataSet = pasarDatosArray(arrayObjetos);

                    if ($('.table-wrapper').html()) {
                        $("#tabla").remove();
                        $('.table-wrapper').html("<table id='tabla'></table >");
                    } else {
                        $('.table-wrapper').html("<table id='tabla'></table >");
                    }

                    crearTable(dataSet);

                    ubicacion = 0;

                },
                    function (error) {

                        let arrayObjetos = [];
                        let dataSet = [];

                        for (let i = 0; i < gasolineras.length; i++) {

                            obj = procesarGasolineras(gasolineras[i])

                            arrayObjetos.push(obj);

                        }
                        dataSet = pasarDatosArray(arrayObjetos);

                        if ($('.table-wrapper').html()) {
                            $("#tabla").remove();
                            $('.table-wrapper').html("<table id='tabla'></table >");
                        } else {
                            $('.table-wrapper').html("<table id='tabla'></table >");
                        }

                        crearTable(dataSet);

                        if (error.code == error.PERMISSION_DENIED)
                            console.log("No se comparte la ubicación");
                    });
            }
            

            let arrayObjetos = [];
            let dataSet = [];

            for (let i = 0; i < gasolineras.length; i++) {

                obj = procesarGasolineras(gasolineras[i])

                arrayObjetos.push(obj);

            }
            dataSet = pasarDatosArray(arrayObjetos);

            if ($('.table-wrapper').html()) {
                $("#tabla").remove();
                $('.table-wrapper').html("<table id='tabla'></table >");
            } else {
                $('.table-wrapper').html("<table id='tabla'></table >");
            }

            crearTable(dataSet);

            //Bucle que recorre el array de gasolineras para obtener los datos
            for (let i = 0; i < gasolineras.length; i++) {

                obj = procesarGasolineras(gasolineras[i])
                mostrarMarcadores(obj.lat, obj.long, map, obj.rotulo, obj.horario, obj.pre95, obj.pre98, obj.preDi, obj.preDiA, 0);

            };
        }
    })
}
/**
 * Funcion que hace una llama al mongo y recibe la fecha y la hora de la ultima actualizacion de los precios
 * @author Cristian Ayala, Marc Nolla, Victor Vera
 * @param  {int} ubicacion - Indica si la ubicación sé está compartiendo o no
 */
function llamarApiMapa(ubicacion) {
    $('#contenedor-mapa').html('<div class="loading"><img src="assets/images/cargaBlanco.gif" alt="loading" /></div>');
    $.ajax({
        url: "https://api.mlab.com/api/1/databases/gasolineras/collections/gasolinera?l=1&s={'FechaHora': -1}&f={'FechaHora': 1}&apiKey=DOZZAytBV3N46Futm-V4gJcR-3SdUIqb",
        headers: {
            "x-requested-with": "xhr"
        },
        ContentType: 'application/json',

        success: function (response) {
            $('input:radio[name=gasolineras]:checked').each(
                function () {
                    actualitzarMarcadors(response[0]["FechaHora"], $(this).val(), ubicacion);
                }
            );
        }
    })
}

$(document).ready(function () {
    window.compartirUbicacion = 0;
    $('#contenedor-mapa').html('<div class="loading"><img src="assets/images/cargaBlanco.gif" alt="loading" /></div>');

    //Obtener la fecha de actualizacion de datos mas reciente y sus gasolineras
    $.ajax({
        url: "https://api.mlab.com/api/1/databases/gasolineras/collections/gasolinera?l=1&s={'FechaHora': -1}&f={'FechaHora': 1}&apiKey=DOZZAytBV3N46Futm-V4gJcR-3SdUIqb",
        headers: {
            "x-requested-with": "xhr"
        },
        ContentType: 'application/json',

        success: function (response) {

            $('input:radio[name=gasolineras]:checked').each(
                function () {
                    actualitzarMarcadors(response[0]["FechaHora"], "todas", 0);
               }
            );
        }
    })

    //Obtiene el historial de precios para el grafico
    $.ajax({
        url: "https://api.mlab.com/api/1/databases/gasolineras/collections/historial?s={'FechaHora': -1}&l=32&apiKey=DOZZAytBV3N46Futm-V4gJcR-3SdUIqb",
        headers: {
            "x-requested-with": "xhr"
        },
        ContentType: 'application/json',

        success: function (response) {
            //Guardamos el array con todas las gasolinares
            let historial = response;

            //Generamos el grafico de barras con las medias del dia actual
            generarGrafico(historial[0]["Media 95"], historial[0]["Media 98"], historial[0]["Media Diesel"], historial[0]["Media Diesel A"]);

            //Generamos el grafico de linias con las medias de los ultimos 6 dias
            generarGrafico2(historial);
        }
    });

    //Escondemos por defecto uno de los graficos al cargar la pagina
    $("#myChart").hide();

    if ($(window).width() < 450) {
        $("#canvas").hide();
        $(".cambiarGrafico").hide();
        $(".tituloGrafico").text("Media actual de los precios");
        $(".cambiarGrafico").text("Evolución de precios");
        $("#myChart").show();
    }

    //Permite cambiar entre los dos graficos haciendo click al boton
    $(".cambiarGrafico").click(function () {
        if ($("#myChart").is(':visible')) {
            $("#myChart").hide();
            $(".tituloGrafico").text("Historial de precios del último mes");
            $(".cambiarGrafico").text("Precios actuales");
            $("#canvas").show();
        } else {
            $("#canvas").hide();
            $(".tituloGrafico").text("Media actual de los precios");
            $(".cambiarGrafico").text("Evolución de precios");
            $("#myChart").show();
        }
    })


    $("input:radio[name=gasolineras]").click(function () {
        if ($(".cambiarTabla").text() == "5 KM") {
            llamarApiMapa(0);
            window.rutaExistente = 0;
        }
        else {
            llamarApiMapa(1);
            window.rutaExistente = 0;
        }
    })

    $(".cambiarTabla").click(function () {
        if ($(".cambiarTabla").text() == "5 KM") {
            window.rutaExistente = 0;
            llamarApiMapa(1);
            $(".cambiarTabla").text("Cataluña");
            $(".tituloTabla").text("Gasolineras cercanas (5 Km)");
        }
        else {
            window.rutaExistente = 0;
            llamarApiMapa(0);
            $(".cambiarTabla").text("5 KM");
            $(".tituloTabla").text("Todas las gasolineras");
        }
    })
});

