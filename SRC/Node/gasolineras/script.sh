#!/bin/bash

HORA=`date +"%H%M"`
horaMinima="0800"
horaMaxima="2359"

if [ "$((10#$HORA))" -ge $((10#$horaMinima)) ] && [ "$((10#$HORA))" -le $horaMaxima ]
then
    wget -O /home/a15vicvercan/public_html/test/listadoGasolineras.txt https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/FiltroCCAA/09
    
    chmod -R o+rx /home/a15vicvercan/public_html/test/
    node /home/a15vicvercan/public_html/gasolineras/SRC/Node/gasolineras/index.js
else
    echo "No Disponible en estas horas"
fi