//DEFINICIÓN DEL ESQUEMA DE LA BASE DE DATOS USANDO MONGOOSE
var mongoose = require('mongoose');

//A la hora de definir el esquema podemos exigir que cumplan un requisito de tipo
//o que sean requeridos o dar valores por defecto
//Este esquema será usado cuando se interactue con la base de datos (en el tirectorio helpers)
var todoSchema = new mongoose.Schema({
    
    FechaHora: {
        type: String,
    },
    "Codigo Postal": {
        type: String,
    },
    Dirección: {
        type: String,
    },
    Horario: {
        type: String,
    },
    Latitud: {
        type: String,
    },
    Localidad: {
        type: String,
    },
    "Longitud (WGS84)": {
        type: String,
    },
    Municipio: {
        type: String,
    },
    "Precio Gasoleo A": {
        type: String,
    },
    "Precio Gasoleo B": {
        type: String,
    },
    "Precio Gasolina 95 Protección": {
        type: String,
    },
    "Precio Gasolina  98": {
        type: String,
    },
    "Precio Nuevo Gasoleo A": {
        type: String,
    },
    Provincia: {
        type: String,
    },
    Rótulo: {
        type: String,
    },
    IDEESS: {
        type: String,
    },
    IDMunicipio: {
        type: String,
    },
    IDProvincia: {
        type: String,
    },
    IDCCAA: {
        type: String,
    },
});

//Creo un modelo mongoose especificando el nombre del modelo, el modelo en si y el nombre
//de la colección que tendrá en Mongo (el nombre de la base la 'tabla')
var Todo = mongoose.model('Gasolineras', todoSchema, 'gasolinera');

module.exports = Todo;