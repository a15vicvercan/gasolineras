window.map = L.map('map');

//Obtener la fecha de hace una semana
let date = new Date();
let dia = date.getDate() - 7;
let mes = date.getMonth();
let año = date.getFullYear();

if(dia.toString().length == 1) {
    dia = "0" + dia;
}

if (mes.toString().length == 1) {
    mes = "0" + mes;
}

window.fechaPasada = dia + "/" + mes + "/" + año;

//Obtener la fecha actual
dia = date.getDate();
mes = date.getMonth();
año = date.getFullYear();

if (dia.toString().length == 1) {
    dia = "0" + dia;
}

if (mes.toString().length == 1) {
    mes = "0" + mes;
}

window.fechaActual = dia + "/" + mes + "/" + año;

//Obtener la hora actual
let hora = date.getHours();
let minutos = date.getMinutes() - 30;

if (hora.toString().length == 1) {
    hora = "0" + hora;
}

if (minutos.toString() < 0) {
    hora--;
    minutos = 59 + (minutos + 1);
}

window.tiempo = hora + ":" + minutos;

console.log(tiempo);

$(document).ready(function () {
    $.ajax({
        url: "https://api.mlab.com/api/1/databases/gasolineras/collections/gasolinera?q={'Fecha':{'$gt':'" + fechaPasada + "'}}&l=480816&apiKey=DOZZAytBV3N46Futm-V4gJcR-3SdUIqb",
        headers: {
            "x-requested-with": "xhr"
        },
        ContentType: 'application/json',

        //q={"date":{"$gt":{"$date":"2017-07-20T23:23:50Z"}}}

        success: function (response) {
            
            //console.log(response);

            if ("geolocation" in navigator) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    window.map.setView([position.coords.latitude, position.coords.longitude], 15);

                    L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    }).addTo(map);

                    L.marker([position.coords.latitude, position.coords.longitude]).addTo(map)
                        .bindPopup('Su posicion actual')

                    //Guardamos el array con todas las gasolinares
                    let gasolineras = response;

                    for (let i = 0; i < gasolineras.length; i++) {

                        if(gasolineras[i]["Fecha"] == fechaActual.toString() && gasolineras[i]["Hora"] > tiempo.toString()) {

                            console.log(gasolineras[i])

                            let lat = gasolineras[i]["Latitud"];
                            lat = lat.replace(",", ".");
                            lat = parseFloat(lat);

                            let long = gasolineras[i]["Longitud (WGS84)"];
                            long = long.replace(",", ".");
                            long = parseFloat(long);

                            //if(lat >= (position.coords.latitude - 0.02) && lat <= (position.coords.latitude + 0.02) && long >= (position.coords.longitude - 0.03) && long <= (position.coords.longitude + 0.03)) {
                            let pre95 = gasolineras[i]["Precio Gasolina 95 Protección"];
                            let pre98 = gasolineras[i]["Precio Gasolina  98"];
                            let preDi = gasolineras[i]["Precio Gasoleo A"];
                            let rotulo = gasolineras[i]["Rótulo"];

                            L.marker([lat, long]).addTo(map)
                                .bindPopup("<b>" + rotulo + "</b><br>Gasolina 95: " + pre95 + "<br>Gasolina 98: " + pre98 + "<br>Diesel: " + preDi);
                            //}

                        }

                    };
                });
            }
        }

    })
});