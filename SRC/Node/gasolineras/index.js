/* Estructura de la aplicación
index.js --> Punto de entrada de la aplicación
 ----> models      directorio con el modelo de la bd usado en la aplicación
 ----> routes      definición de las rutas de la api
 ----> helpers     métodos auxiliares que implementan las rutas de la api
 */

var db = require("./models");

var axios = require('axios');

//Las rutas de nuestra aplicación estan definidas en el directorio routes, en el fichero todos.js
var todoRoutes = require("./routes/todos");

//Llamada AXIOS que devuelve un json con todas las gasolineras y sus precios actuales
axios.get('http://labs.iam.cat/~a15vicvercan/test/listadoGasolineras.txt', {
})
  .then(function (response) {

    console.log("Tinc les dades");

    //Guardamos la fecha actual
    let date = new Date();
    let dia = date.getDate();
    let mes = date.getMonth() + 1;
    let año = date.getFullYear();

    if (dia.toString().length == 1) {
      dia = "0" + dia;
    }

    if (mes.toString().length == 1) {
      mes = "0" + mes;
    }

    //Guardamos la hora actual
    let hora = date.getHours();
    let minutos = date.getMinutes();

    if (hora.toString().length == 1) {
      hora = "0" + hora;
    }

    if (minutos.toString().length == 1) {
      minutos = "0" + minutos;
    }

    let fecha = { 'FechaHora': año + mes + dia + hora + minutos };

    //Guardamos el array con todas las gasolinares
    let gasolineras = response.data.ListaEESSPrecio;

    for (let i = 0; i < gasolineras.length; i++) {

      //Guardamos el codigo postal
      let codigoPostal = { 'Codigo Postal': gasolineras[i]["C.P."] };

      Object.assign(gasolineras[i], fecha)
      Object.assign(gasolineras[i], codigoPostal)

      if (i == gasolineras.length - 1) {
        db.Todo.create(gasolineras[i]).then(result => process.exit(0));
      }
      else {
        db.Todo.create(gasolineras[i]);
      }

    }

  })
  .catch(function (error) {
    console.log(error);
  });
