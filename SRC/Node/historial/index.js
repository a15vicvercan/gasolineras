/* Estructura de la aplicación
index.js --> Punto de entrada de la aplicación
 ----> models      directorio con el modelo de la bd usado en la aplicación
 ----> routes      definición de las rutas de la api
 ----> helpers     métodos auxiliares que implementan las rutas de la api
 */

var db = require("./models");

var axios = require('axios');

//Las rutas de nuestra aplicación estan definidas en el directorio routes, en el fichero todos.js
var todoRoutes = require("./routes/todos");

//Llamada AXIOS que devuelve un json con todas las gasolineras y sus precios actuales
axios.get('https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/FiltroCCAA/09', {
})
  .then(function (response) {

    //Guardamos la fecha actual
    let date = new Date();
    let dia = date.getDate();
    let mes = date.getMonth()+1;
    let año = date.getFullYear();

    if(dia.toString().length == 1) {
      dia = "0" + dia;
    }

    if(mes.toString().length == 1) {
      mes = "0" + mes;
    }

    //Guardamos la hora actual
    let hora = date.getHours();
    let minutos = date.getMinutes();

    if(hora.toString().length == 1) {
      hora = "0" + hora;
    }

    if(minutos.toString().length == 1) {
      minutos = "0" + minutos;
    }

    let fecha = { "FechaHora":  año + mes + dia + hora + minutos};

    //Guardamos el array con todas las gasolinares
    let gasolineras = response.data.ListaEESSPrecio;

    let media95 = 0;
    let media98 = 0;
    let mediaDi = 0;
    let mediaDiA = 0;

    let gasolinera95 = gasolineras.length;
    let gasolinera98 = gasolineras.length;
    let gasolineraDi = gasolineras.length;
    let gasolineraDiA = gasolineras.length;

    //Bucle que recorre todo el objeto de gasolineras
    for (let i = 0; i < gasolineras.length; i++) {

      let precio95 = gasolineras[i]["Precio Gasolina 95 Protección"];
      let precio98 = gasolineras[i]["Precio Gasolina  98"];
      let precioDi = gasolineras[i]["Precio Gasoleo A"];
      let precioDiA = gasolineras[i]["Precio Nuevo Gasoleo A"];

      if (precio95 != null) {
        precio95 = precio95.replace(",", ".");
        media95 = media95 + parseFloat(precio95);
      } else {
        gasolinera95--;
      }

      if (precio98 != null) {
        precio98 = precio98.replace(",", ".");
        media98 = media98 + parseFloat(precio98);
      } else {
        gasolinera98--;
      }

      if (precioDi != null) {
        precioDi = precioDi.replace(",", ".");
        mediaDi = mediaDi + parseFloat(precioDi);
      } else {
        gasolineraDi--;
      }

      if (precioDiA != null) {
        precioDiA = precioDiA.replace(",", ".");
        mediaDiA = mediaDiA + parseFloat(precioDiA);
      } else {
        gasolineraDiA--;
      }
    }

    media95 = media95 / gasolinera95;
    media98 = media98 / gasolinera98;
    mediaDi = mediaDi / gasolineraDi;
    mediaDiA = mediaDiA / gasolineraDiA;

    let precio95 = {"Media 95" : media95.toString()};
    let precio98 = {"Media 98" : media98.toString()};
    let precioDi = {"Media Diesel" : mediaDi.toString()};
    let precioDiA = {"Media Diesel A" : mediaDiA.toString()};
  
    let obj = {}

    Object.assign(obj, precio95);
    Object.assign(obj, precio98);
    Object.assign(obj, precioDi);
    Object.assign(obj, precioDiA);
    Object.assign(obj, fecha);

    db.Todo.create(obj).then(result => process.exit(0));
    
  })
  .catch(function (error) {
    console.log(error);
  });
