//Cargamos Mongoose, una libreria para facilitar el trabajo con Mongo
var mongoose = require('mongoose');
mongoose.set('debug', true);

let url = 'mongodb://admin:ausias_1234@ds125945.mlab.com:25945/gasolineras';
//Establecemos la conexion con la base de datos todo-api en nuestro servidor mlab
mongoose.connect(url, {useMongoClient: true});
console.log(url);
//Habilitamos el uso de Promises para las llamadas a Mongo
mongoose.Promise = Promise;

module.exports.Todo = require("./todo");